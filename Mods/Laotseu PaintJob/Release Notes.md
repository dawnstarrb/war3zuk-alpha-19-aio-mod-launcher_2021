**PaintJob** Change the colour of FinPims vehicles.

![PaintJob](https://github.com/Laotseu/7dtdMods/blob/master/PaintJob/PaintJob.png)

**Important:** For this modlet to work properly, it needs to be installed on the **server** and **every player's computer**.

* 1.12: Bicycle and motorcycle
	- Bicycles: white, blue, green, cyan, pink red, and the original yellow
	- Motorcycles: red, white, and original
* 1.11: To much bumps on the road...
	- Fix UK models wobble wheels
	- More prep work for the motorcycles
* 1.10: War3zUK skin
	- It came to my attention that UK also have crazy driving habits. They might actually be the originators of 
	  the whole darn thing.
* 1.9: Add the missing unity3D package for the Australian jeep
* 1.8: There is now a jeep for people that drive on the wrong side of the road
	- Fix a problem with the windshields textures
	- New Patrol Silver and Blue, Autralian style
* 1.7: Fix bugs reported by MrGrim and GunMuse (thanks)
	- The A19 max speed is 14m/s. Vroom, vroom 
	- Try to make the Jeeps more bridge friendly
	- Add missing Pink Rider icon
* 1.6: Mesh file fixes
	- Plug a hole on the left side of the front bumper
	- Move the steering wheel forward to leave more space for driver's avatar* 1.5: Add Pink Rider 4x4 truck
* 1.5: Add Pink Rider
* 1.4: Add missing hud sprite for the Jeeps
* 1.3: Rebuild assets for the new linear lighting (Alpha 19)
* 1.2: The blender release 
	- 4x4 top lights can now be turned on
	- The meshes and rigs are now comming from Blender
	- Worked on the physics of the 4x4 (they now stop properly)
* 1.1: Pimp your motorcycle (test pattern)
* 1.0: Ride in style! Change the colour of your 4x4
	- Add three 4x4 patterns: scrubed, blue, red, and Spartan black and gold
	- Add recipes to scrubed the original 4x4
	- Add recipes to paint the scrubed 4x4	

* Thanks to Manux for headlight that actually light up.

[click here for Modlet Instalation instruction](https://github.com/Laotseu/7dtdMods/blob/master/Modlet%20Installation.md)
[Click here for a list of all my mods](https://github.com/Laotseu/7dtdMods/blob/master/README.md)
